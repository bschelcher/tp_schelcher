#ifndef TRIANGLE_H
#define TRIANGLE_H
#include "Point.h"
class Triangle
{
public:
    //Constructeur
    Triangle();
    //Getter Setter point
    inline Point GetPointA(){return m_pointA;}
    inline void SetPointA(const Point pointA){m_pointA = pointA;}
    inline Point GetPointB(){return m_pointB;}
    inline void SetPointB(const Point pointB){m_pointB = pointB;}
    inline Point GetPointC(){return m_pointC;}
    inline void SetPointC(const Point pointC){m_pointC = pointC;}

    //Getter Setter Longueur
    inline double GetLongueurAtoB(){return m_longueurAtoB;}
    inline void SetLongueurAtoB(const double longueurAtoB){m_longueurAtoB = longueurAtoB;}
    inline double GetLongueurBtoC(){return m_longueurBtoC;}
    inline void SetLongueurBtoC(const double longueurBtoC){m_longueurBtoC = longueurBtoC;}
    inline double GetLongueurAtoC(){return m_longueurAtoC;}
    inline void SetLongueurAtoC(const double longueurAtoC){m_longueurAtoC = longueurAtoC;}

    inline double GetLongueurMin(){return m_longueurMin;}
    inline void SetLongueurMin(const double longueurMin){m_longueurMin = longueurMin;}
    inline double GetLongueurMid(){return m_longueurMid;}
    inline void SetLongueurMid(const double longueurMid){m_longueurMid = longueurMid;}
    inline double GetLongueurMax(){return m_longueurMax;}
    inline void SetLongueurMax(const double longueurMax){m_longueurMax = longueurMax;}

    //Calcul longueur
    void longeur();
    void tri();

    //Calcul hauteur
    double hauteur();

    //Calcul base
    double base();

    //Calcul surface
    double surface();

    //Test
    bool isocele();
    bool rectangle();
    bool equilateral();


    void Affiche();
private:
    //Points
    Point m_pointA;
    Point m_pointB;
    Point m_pointC;
    //Longueur
    double m_longueurAtoB;
    double m_longueurBtoC;
    double m_longueurAtoC;
    double m_longueurMax;
    double m_longueurMid;
    double m_longueurMin;

};

#endif // TRIANGLE_H
