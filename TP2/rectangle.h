#ifndef RECTANGLE_H
#define RECTANGLE_H
#include "Point.h"


class Rectangle
{
public:
    Rectangle();
    double perimetre();
    double surface();

    void afficher();

    inline int GetLongueur(){return m_longueur;}
    inline void SetLongueur(const int longueur){m_longueur = longueur;}
    inline int GetLargeur(){return m_largeur;}
    inline void SetLargeur(const int largeur){m_largeur = largeur;}


private:
     int m_longueur;
     int m_largeur;
     Point coin_Superieur_Gauche;

};

#endif // RECTANGLE_H
