#include "Triangle.h"
#include<math.h>
#include<iostream>
using namespace std;

Triangle::Triangle()
{
    Affiche();
}
void Triangle::longeur()
{
    m_longueurAtoB = (sqrt(pow(m_pointB.x -m_pointA.x,2)+pow(m_pointB.y -m_pointA.y,2)));
    m_longueurBtoC = sqrt(pow(m_pointC.x -m_pointB.x,2)+pow(m_pointC.y -m_pointB.y,2));
    m_longueurAtoC = sqrt(pow(m_pointC.x -m_pointA.x,2)+pow(m_pointC.y -m_pointA.y,2));
}
void Triangle::tri()
{


    double tab[3] = {m_longueurAtoB,m_longueurBtoC,m_longueurAtoC};

      int i=0;
      double tmp=0;
      int j=0;

      for(i = 0; i < 3; i++)
        {
          for(j = i+1; j < 3; j++)
          {
              if(tab[j] < tab[i])
                {
                  tmp = tab[i];
                  tab[i] = tab[j];
                  tab[j] = tmp;
                }
            }
        }

    m_longueurMax = tab[2];
    m_longueurMin = tab[0];
    m_longueurMid = tab[1];
}
double Triangle::hauteur()
{
    return sqrt(pow(m_longueurMin,2) - pow((pow(m_longueurMax,2)-pow(m_longueurMid,2)+pow(m_longueurMin,2))/(2*m_longueurMax),2));

}
double Triangle::base()
{
    return m_longueurMax;
}
double Triangle::surface()
{
    return (base()*hauteur())/2;
}
//Test
bool Triangle::isocele()
{


    if(m_longueurAtoB==m_longueurBtoC || m_longueurAtoB==m_longueurAtoC || m_longueurBtoC==m_longueurAtoC)return true;
    else return false;
}
bool Triangle::rectangle()
{
    if(pow((m_longueurMid+m_longueurMin),2)==pow(m_longueurMax,2))return true;
    else return false;
}
bool Triangle::equilateral()
{
if(m_longueurAtoB==m_longueurBtoC && m_longueurAtoB==m_longueurAtoC)return true;
else return false;
}

void Triangle::Affiche()
{
    std::cout<<"Entrez la valeur des points"<<endl;

            std::cout<<"Entrez le point A"<<endl;
            std::cin>>m_pointA.x >> m_pointA.y;
            std::cout<<"Entrez le point B"<<endl;
            std::cin>>m_pointB.x >> m_pointB.y;
            std::cout<<"Entrez le point A"<<endl;
            std::cin>>m_pointC.x >> m_pointC.y;
            longeur();
            tri();
            std::cout<<"La longueur de A a B : "<<m_longueurAtoB<<endl;
            std::cout<<"La longueur de A a C : "<<m_longueurAtoC<<endl;
            std::cout<<"La longueur de B a C : "<<m_longueurBtoC<<endl;
            std::cout<<"La base : "<<base()<<endl;
            std::cout<<"La hauteur : "<<hauteur()<<endl;
            std::cout<<"Surface : "<<surface()<<endl;
            if(rectangle()==true)std::cout<<"Le triangle est rectangle"<<endl;
            else std::cout<<"Le triangle n'est pas rectangle"<<endl;
            if(isocele()==true)std::cout<<"Le triangle est isocele"<<endl;
            else std::cout<<"Le triangle n'est pas isocele"<<endl;
            if(equilateral()==true)std::cout<<"Le triangle est equilateral"<<endl;
            else std::cout<<"Le triangle n'est pas equilateral"<<endl;


}
