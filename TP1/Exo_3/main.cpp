#include <iostream>

using namespace std;

int choix;
int x,y;
int result;
int somme_p(int a, int b);
int somme_r(int *a, int* b);

int main()
{
    cout << "Choisissez entre la methode reference(1) et la methode pointeur(2)" << endl;
    cin >> choix;
    cout << " Choisissez deux nombre a et b "<< endl;
    cin >> x >> y;
    if(choix == 2)
    {
       int *x = x;
       int *y = y;
        result = somme_p(*x,*y);
        cout << " pointeur : " << result << endl;
    }
    else if (choix == 1)
    {
        result= somme_r(&x,&y);
        cout << " Reference : " << result << endl;
    }
    else
    {
        return 0;
    }
    return 0;
}

int somme_p(int a, int b)
{
    return a + b;
}
int somme_r(int* a, int* b)
{
    int e = *a + *b;
    return e;
}
