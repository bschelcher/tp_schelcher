TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
        grillemorpion.cpp \
        grillepuissance4.cpp \
        jeu.cpp \
        main.cpp

HEADERS += \
    caseStruct.h \
    grillemorpion.h \
    grillepuissance4.h \
    jeu.h
