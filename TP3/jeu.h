#ifndef JEU_H
#define JEU_H


#include "caseStruct.h"
#include "grillemorpion.h"
#include "grillepuissance4.h"


class jeu
{
// Déclaration des attributs et méthodes publiques
public:
    jeu();

    void LancerJeuMorpion();
    void LancerJeuP4();


// Déclaration des attributs et méthodes privés
private :
    int tourJoueur;
    bool jeuFini;
    int numTour;
    GrilleMorpion currentGrille;
    GrillePuissance4 currentGrilleP4;


    void resetGrille();


    caseStruct demandeCase() const;
    int DemanderColonne() const;
    bool ColonnePleine(int numCol);

    int demanderJoueurTour1();



    void nextTourJoueur();



};
#endif // JEU_H
