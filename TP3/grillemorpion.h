#ifndef GRILLEMORPION_H
#define GRILLEMORPION_H
#include "caseStruct.h"

class GrilleMorpion
{
// Déclaration des attributs et méthodes publiques
public:
    GrilleMorpion();

    bool CaseVide(caseStruct caseTest) const;

    void DeposeJeton(caseStruct caseDepot, int numJoueur);

    bool VictoireJoueur(int numJoueur) const;

    void AfficherGrille();

    void resetGrille();



// Déclaration des attributs et méthodes privées
private:
    int grille [3][3] = {{0,0,0}, {0,0,0}};





    bool LigneComplete(int numLigne ,int numJoueur) const;

    bool ColonneComplete(int numCol, int numJoueur) const;

    bool DiagonaleComplete(int numDiag, int numJoueur) const;

};
#endif // GRILLEMORPION_H
