#include "jeu.h"
#include "grillemorpion.h"
#include<iostream>

using namespace std;

// Constructeur
jeu::jeu()
{
    this->jeuFini = false;
    this->numTour = 0;
    this->tourJoueur = 0;
}

//Debute le jeu Morpion
void jeu::LancerJeuMorpion(){

    caseStruct currentCase;
    bool rejouer = false;
    bool numRejValide = false;
    bool draw = false;
    int numRejouer = 0;
    this->numTour = 1;




    do {
             this->tourJoueur = demanderJoueurTour1();
        do {

               do {
                    currentCase = demandeCase();

                    if (this->currentGrille.CaseVide(currentCase) ){

                        cout<<"Case vide : vous pouvez poser un pion !"<<endl;
                    }else {

                        cout<<"Case pleine : demandez une autre case !"<<endl;
                    }

               }while (!this->currentGrille.CaseVide(currentCase));

                this->currentGrille.DeposeJeton(currentCase, this->tourJoueur);
                this->currentGrille.AfficherGrille();

                if (this->currentGrille.VictoireJoueur( tourJoueur)){
                    jeuFini = true;

                }

                if(this->numTour == 9){
                    jeuFini = true;
                }

                if (!jeuFini){
                    nextTourJoueur();
                    this->numTour++;
                }


        }while(!this->jeuFini );
        if (numTour == 9){
            draw = true;
            do {
                cout<<"Vous avez fait égalité : rejouer ? (Oui : 1 | Non : 0) ";
                cin>>numRejouer;

                if (numRejouer == 1){
                    rejouer = true;
                    numTour = 1;
                    numRejValide = true;
                }else if (numRejouer == 0) {
                    rejouer = false;
                    numTour = 1;
                    numRejValide = true;
                }else {
                    cout<<"Numero invalide : veuillez en resaisir un"<<endl;
                }

            }while (!numRejValide);

            if(rejouer == true){
                draw = false;
                currentGrille.resetGrille();
                jeuFini = false;
            }
        }

        if (jeuFini && !draw){
            cout<<"Felicitations Joueur "<<this->tourJoueur<<" pour ta victoire "<<endl;
        }

    }while (rejouer);
}

//Debute le jeu puissance4
void jeu::LancerJeuP4(){
    bool rejouer = false;
    bool draw = false;
    bool saisieColoneValide = false;
    bool numRejValide = false;
    int currentColonne = 0;
    int numRejouer = 0;
    this->numTour = 1;

    do {
        this->tourJoueur = demanderJoueurTour1();
        do{

            do {
                 saisieColoneValide = false;
                 currentColonne = DemanderColonne();

                 if (!this->currentGrilleP4.ColonnePleine(currentColonne)){
                     cout<<"Colonne vide : vous pouvez poser un pion !"<<endl;
                     saisieColoneValide = true;
                 }else {
                     cout<<"Colonne pleine : demandez une autre case !"<<endl;
                 }

            }while (!saisieColoneValide);

            this->currentGrilleP4.DeposeJeton(currentColonne, this->tourJoueur);
            this->currentGrilleP4.AfficherGrille();

            if (this->currentGrilleP4.VictoireJoueur(tourJoueur)){
                jeuFini = true;
            }

            if(this->numTour == 28){
                jeuFini = true;
            }

            if (!jeuFini){
                nextTourJoueur();
                this->numTour++;
            }


        }while(!this->jeuFini);

        if (numTour == 28){
            draw = true;
            do {
                cout<<"Vous avez fait égalité : rejouer ? (Oui : 1 | Non : 0) ";
                cin>>numRejouer;

                if (numRejouer == 1){
                    rejouer = true;
                    numTour = 1;
                    numRejValide = true;
                }else if (numRejouer == 0) {
                    rejouer = false;
                    numTour = 1;
                    numRejValide = true;
                }else {
                    cout<<"Numero invalide : veuillez en resaisir un"<<endl;
                }

            }while (!numRejValide);

            if(rejouer == true){
                draw = false;
                jeuFini = false;
            }
        }

        if (jeuFini && !draw){
            cout<<"Felicitations Joueur "<<this->tourJoueur<<" pour ta victoire "<<endl;
        }

    }while (rejouer);


}

//

//Demande sur quelle colonne le joueur veut placer son pion
int jeu::DemanderColonne() const {
    bool colonneValide = false;
    int colonneDonnee;
    cout<<"Joueur "<<this->tourJoueur<<" : "<<endl;
    do{
        cout<<"Sur quelle colonne voulez vous placer ? : ";
        cin>>colonneDonnee;

        if (colonneDonnee >= 1 && colonneDonnee <= 7){
            colonneValide = true;
        }

        if (!colonneValide){
            cout<<"Numero invalide : veuillez en resaisir un"<<endl;
        }

    }while(!colonneValide);
    return colonneDonnee-1;
}

//Demande sur quelle ligne le joueur veut placer son pion
caseStruct jeu::demandeCase() const{
    caseStruct caseDonnee;
    bool ligneValide = false;
    bool colonneValide = false;
    cout<<"Joueur "<<this->tourJoueur<<" : "<<endl;
    do{
        cout<<"Sur quelle ligne voulez vous placer ? : ";
        cin>>caseDonnee.numLigne;

        if (caseDonnee.numLigne == 1 ){
            ligneValide = true;
        }else if (caseDonnee.numLigne  == 2){
            ligneValide = true;
        }else if (caseDonnee.numLigne  == 0){
            ligneValide = true;
        }

        if (!ligneValide){
            cout<<"Numero invalide : veuillez en resaisir un"<<endl;
        }

    }while(!ligneValide);


    do{
        cout<<"Sur quelle colonne voulez vous placer ? : ";
        cin>>caseDonnee.numColonne;

        if (caseDonnee.numColonne == 1 ){
            colonneValide = true;
        }else if (caseDonnee.numColonne  == 2){
            colonneValide = true;
        }else if (caseDonnee.numColonne  == 0){
            colonneValide = true;
        }

        if (!colonneValide){
            cout<<"Numero invalide : veuillez en resaisir un"<<endl;
        }

    }while(!colonneValide);
    return caseDonnee;
}
//Demande quel joueur commencera
int jeu::demanderJoueurTour1(){
    int numJoueurTour = 0;
    bool numValide = false;

    do{
         std::cout<<"Quel joueur commencera la partie ? : ";
         std::cin>>numJoueurTour;
         if (numJoueurTour == 1 ){
             numValide = true;
         }else if (numJoueurTour == 2){
             numValide = true;
         }

     }while(!numValide);





    return numJoueurTour;
}

//Passe au Tour suivant
void jeu::nextTourJoueur(){
    if(this->tourJoueur == 1){
        this->tourJoueur = 2;
    }else if(this->tourJoueur == 2){
        this->tourJoueur = 1;
    }
}

