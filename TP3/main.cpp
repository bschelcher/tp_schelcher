#include <iostream>
#include "grillemorpion.h"
#include "grillepuissance4.h"
#include "jeu.h"

using namespace std;

int main()
{
    // Déclaration du jeu
    jeu j;
    // Déclaration et initialisation du choix du joueur
    int choix = 0;

    // Boucle de vérificaiton de validité du choix du joueur
    do {
        // Affichage du menu

        cout<<"A quel jeu voulez vous jouer ? : "<<endl;
        cout<<"\n\t Morpion(1) ";
        cout<<"\n\t Puissance4(2) ";
        cout<<"\n\n\t Choix :  ";
        // Saisie du choix
        cin>>choix;

    }while(choix != 1 && choix != 2);

    switch (choix) {

        // Si 1 lancement du morpion
        case 1 :
            j.LancerJeuMorpion();
        break;

        // Si 2 lancement du puissance 4
        case 2 :
            j.LancerJeuP4();
        break;

        // Traitement d'un choix invalide
        default :
            cout<<"Invalide"<<endl;
        break;
    }

    return 0;
}
