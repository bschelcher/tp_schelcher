#include "grillemorpion.h"
#include "caseStruct.h"
#include <iostream>

using namespace std;


GrilleMorpion::GrilleMorpion()
{

}

bool GrilleMorpion::CaseVide(caseStruct caseTest) const {
    return (this->grille[caseTest.numLigne][caseTest.numColonne] == 0);
}
void GrilleMorpion::DeposeJeton(caseStruct caseDepot, int numJoueur){

    if (CaseVide(caseDepot)){
        this->grille[caseDepot.numLigne][caseDepot.numColonne] = numJoueur;
    }
}

bool GrilleMorpion::LigneComplete(int numLigne, int numJoueur) const{

    for (int i=0 ; i<3; i++){
        if (this->grille[numLigne][i] != numJoueur){
            return false;
        }
    }
    std::cout<<"Ligne " << numLigne << " complète par le joueur : " << numJoueur<<endl;
    return true;

}


bool GrilleMorpion::ColonneComplete(int numCol, int numJoueur) const{

    for (int i=0 ; i<3; i++){
        if (this->grille[i][numCol] != numJoueur){
            return false;
        }
    }
    std::cout<<"Colonne " << numCol << " complète par le joueur : " << numJoueur<<endl;
    return true;

}

bool GrilleMorpion::DiagonaleComplete(int numDiag, int numJoueur) const {

    if (numDiag == 0){
        for (int i = 0; i<3; i++){
            if (this->grille[i][i] != numJoueur){
                return false;
            }
        }
        cout<<"Diagonale complète par le joueur : " << numJoueur<<endl;
        return true;

    }else if(numDiag == 2){
        for (int i = 0; i<3; i++){
            if (this->grille[numDiag][i] != numJoueur){
                return false;
            }
            numDiag -=1;
        }
        cout<<"Diagonale complète par le joueur : " << numJoueur<< endl;
        return true;


    }else {
        std::cout<<"Numéro de diagonale invalide"<<endl;
    }

    return false;
}

bool GrilleMorpion::VictoireJoueur(int numJoueur) const{
    return (LigneComplete(0,numJoueur) || LigneComplete(1,numJoueur) || LigneComplete(2,numJoueur)
            || ColonneComplete(0,numJoueur) || ColonneComplete(1,numJoueur) || ColonneComplete(2,numJoueur)
            || DiagonaleComplete(0,numJoueur) || DiagonaleComplete(2,numJoueur));
}

void GrilleMorpion::AfficherGrille(){

    char sym;

    cout<<" Affichage de la grille : " << endl;


    for (int i = 0; i<3 ; i++){
        cout<<" [  " ;
        for (int j=0; j<3 ; j++){
            switch (this->grille[i][j]) {

            case 0 :
                sym = '-';

                break;
            case 1 :
                sym = 'X';
                break;
            case 2 :
                sym = 'O';
                break;
            }
            cout<<sym<< "  ";
        }
        cout<<"] " << endl;
    }

}

void GrilleMorpion::resetGrille(){
    for (int i = 0; i<3 ; i++){

        for (int j=0; j<3 ; j++){
            this->grille[i][j] = 0;
        }

    }
}
