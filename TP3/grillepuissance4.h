#ifndef GRILLEPUISSANCE4_H
#define GRILLEPUISSANCE4_H


class GrillePuissance4
{
// Déclaration des attributs et méthodes publiques
public:
    GrillePuissance4();

    bool CaseVide(int numL, int numC) const;

    bool ColonnePleine(int numCol)const;

    void DeposeJeton(int ColonneDepot, int numJoueur);

    bool VictoireJoueur(int numJoueur) const;

    void AfficherGrille();

// Déclaration des attributs et méthodes privées
private :
    int grilleP4 [4][7] = {{0,0,0,0,0,0,0}, {0,0,0,0,0,0,0},{0,0,0,0,0,0,0},{0,0,0,0,0,0,0}};

    bool LigneVictoire(int numJ) const;

    bool DiagonaleVictoire(int numJ) const;

    bool ColonneVictoire (int numJ) const;
};

#endif // GRILLEPUISSANCE4_H
