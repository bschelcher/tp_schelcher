#include "grillepuissance4.h"
#include <iostream>

using namespace std;


GrillePuissance4::GrillePuissance4()
{

}


bool GrillePuissance4::CaseVide(int numL, int numC) const {
    return (this->grilleP4[numL][numC] == 0);
}
bool GrillePuissance4::ColonnePleine(int numCol)const{
    for (int i=0 ; i<=4 ;i ++){
        if(this->CaseVide(i, numCol)){
            return false;
        }
    }
    return true;
}

void GrillePuissance4::DeposeJeton(int ColonneDepot, int numJoueur){

    int currentLigne = 3;
    bool caseValide = false;

    while (!caseValide && currentLigne>=0) {
        if(this->CaseVide(currentLigne, ColonneDepot)){
            this->grilleP4[currentLigne][ColonneDepot] = numJoueur;
            caseValide = true;
        }
        currentLigne--;
    }

}

void GrillePuissance4::AfficherGrille(){

    char sym;

    cout<<" Affichage de la grille : " << endl;

    for (int i = 0; i<=3 ; i++){
        cout<<" [  " ;
        for (int j=0; j<=6 ; j++){
            switch (this->grilleP4[i][j]) {

            case 0 :
                sym = '-';

                break;
            case 1 :
                sym = 'X';
                break;
            case 2 :
                sym = 'O';
                break;
            }
            cout<<sym<< "  ";
        }
        cout<<"] " << endl;
    }

}


bool GrillePuissance4::LigneVictoire(int numJ) const {

    for (int i=0 ; i<= 3 ; i++){

        for (int j=0 ; j<= 3 ; j++){
            if(
                (this->grilleP4[i][j] == numJ) &&
                (this->grilleP4[i][j+1] == numJ) &&
                (this->grilleP4[i][j+2] == numJ) &&
                (this->grilleP4[i][j+3] == numJ) ){
                return true;
            }
        }
    }
    return false;

}

bool GrillePuissance4::DiagonaleVictoire (int numJ) const{

    // Test des diagonales de gauche à droite
    for (int i = 0; i<4;i++) {

        if (
                (this->grilleP4[0][i] == numJ) &&
                (this->grilleP4[1][i+1] == numJ) &&
                (this->grilleP4[2][i+2] == numJ) &&
                (this->grilleP4[3][i+3] == numJ) ){
                return true;
        }

    }

    // Test des diagonales de droite à gauche
    for (int j = 6; j>2;j--) {

        if (
                (this->grilleP4[0][j] == numJ) &&
                (this->grilleP4[1][j-1] == numJ) &&
                (this->grilleP4[2][j-2] == numJ) &&
                (this->grilleP4[3][j-3] == numJ) ){
                return true;
        }

    }

    return false;
}

bool GrillePuissance4::ColonneVictoire (int numJ) const{

    for (int j=0 ; j<= 6 ; j++){
        if(this->ColonnePleine(j)){
            if (
                    (this->grilleP4[0][j] == numJ) &&
                    (this->grilleP4[1][j] == numJ) &&
                    (this->grilleP4[2][j] == numJ) &&
                    (this->grilleP4[3][j] == numJ) ){
                    return true;
            }

        }
    }
    return false;
}

bool GrillePuissance4::VictoireJoueur(int numJoueur) const{
    return (LigneVictoire(numJoueur) || ColonneVictoire(numJoueur) || DiagonaleVictoire(numJoueur) );
}
